//
//  Survival.swift
//  p06-atari
//
//  Created by Souritra Das Gupta on 5/10/17.
//  Copyright © 2017 Souritra Das Gupta. All rights reserved.
//

import Foundation
import SpriteKit
import GameplayKit

var score11 = 0
class Survival: SKScene, SKPhysicsContactDelegate {
    let p1 = SKSpriteNode(color: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), size: CGSize(width: 200, height: 20))
    let p2 = SKSpriteNode(color: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), size: CGSize(width: 200, height: 20))
    let ball = SKSpriteNode(imageNamed: "ball")
    let scorelabel1 = SKLabelNode(fontNamed: "Pixel-Noir Caps")

    
    override func didMove(to view: SKView) {
        score11 = 0;
        self.physicsWorld.contactDelegate = self
        let inflabel = SKLabelNode(fontNamed: "Pixel-Noir Caps")
        inflabel.text = "Survive!"
        inflabel.fontSize = 45
        inflabel.fontColor = #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
        inflabel.position = CGPoint(x: self.frame.size.width/2, y: self.frame.size.height/2 + 45)
        scorelabel1.text = "0"
        scorelabel1.fontSize = 25
        scorelabel1.fontColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        scorelabel1.fontSize = 25
        scorelabel1.fontColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        scorelabel1.position = CGPoint(x: self.size.width/2, y: self.size.height/2 - 45)
        scorelabel1.zPosition = 10
        self.addChild(scorelabel1)
        let background = SKSpriteNode(color: #colorLiteral(red: 0.05882352963, green: 0.180392161, blue: 0.2470588237, alpha: 1), size: self.frame.size)
        background.position = CGPoint(x: 0, y: 0)
        background.anchorPoint = CGPoint(x: 0, y: 1.0)
        addChild(background)
        background.addChild(inflabel)
        inflabel.run(bouncybouncy)
        inflabel.run(SKAction.fadeOut(withDuration: 2.5))
        let border = SKPhysicsBody(edgeLoopFrom: self.frame)
        border.friction = 0
        border.restitution = 1
        border.categoryBitMask = 3
        border.collisionBitMask = 2 | 1
        self.physicsBody = border
        p1.position = CGPoint(x: self.frame.size.width/2, y: 65)
        p2.position = CGPoint(x: self.frame.size.width/2, y: self.frame.size.height - 65)
        p1.physicsBody = SKPhysicsBody(rectangleOf: p1.size)
        p2.physicsBody = SKPhysicsBody(rectangleOf: p2.size)
        p1.physicsBody?.categoryBitMask = 1
        p2.physicsBody?.categoryBitMask = 1
        p1.physicsBody?.collisionBitMask = 2 | 3
        p2.physicsBody?.collisionBitMask = 2 | 3
        p1.physicsBody?.isDynamic = false
        p2.physicsBody?.isDynamic = false
        p1.physicsBody?.friction = 0
        p1.physicsBody?.restitution = 1
        p2.physicsBody?.friction = 0
        p2.physicsBody?.restitution = 1
        
        self.addChild(p1)
        self.addChild(p2)
        ball.size = CGSize(width: 20, height: 20)
        ball.physicsBody = SKPhysicsBody(circleOfRadius: 10)
        ball.position = CGPoint(x: self.frame.size.width/2 - 5, y: self.frame.size.height/2 + 7)
        ball.physicsBody?.affectedByGravity = false
        ball.physicsBody?.categoryBitMask = 2
        ball.physicsBody?.collisionBitMask = 1
        ball.physicsBody?.linearDamping = 0
        ball.physicsBody?.angularDamping = 0
        ball.physicsBody?.friction = 0
        ball.physicsBody?.restitution = 1
        self.addChild(ball)
        ball.physicsBody?.applyImpulse(CGVector(dx: 10, dy: -10))
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t: AnyObject in touches{
            let touchpos = t.location(in: self)
            let prevtouchpos = t.previousLocation(in: self)
            let movement = touchpos.x - prevtouchpos.x
                p1.position.x += movement
                p2.position.x += movement
        }
    }
    
    func gameover()
    {
        let toScene = GOSurvival(size: self.size)
        toScene.scaleMode = self.scaleMode
        let move = SKTransition.fade(withDuration: 1)
        self.view!.presentScene(toScene, transition: move)
    }
    
    override func update(_ currentTime: TimeInterval) {
        p2.run(SKAction.moveTo(x: ball.position.x, duration: 0.5))
        if ball.position.y < p1.position.y - 10{
            ball.position = CGPoint(x: self.frame.size.width/2, y: self.frame.size.height/2 + 7)
            ball.physicsBody?.velocity = CGVector(dx: 0, dy: 0)
            p1.run(SKAction.scaleX(to: p1.xScale * 0.8, duration: 0.1))
            ball.physicsBody?.applyImpulse(CGVector(dx: 10, dy: -10))
        }
        else if ball.position.y > p2.position.y + 10{
            ball.position = CGPoint(x: self.frame.size.width/2, y: self.frame.size.height/2 + 7)
            ball.physicsBody?.velocity = CGVector(dx: 0, dy: 0)
            score11 += 1
            scorelabel1.text = "\(score11)"
            scorelabel1.run(bouncybouncy)
            ball.physicsBody?.applyImpulse(CGVector(dx: -10, dy: 10))
        }
        
        if(p1.xScale <= 0.75 && p1.xScale > 0.4)
        {
            p1.color = #colorLiteral(red: 0.9607843161, green: 0.7058823705, blue: 0.200000003, alpha: 1)
        }
        else if(p1.xScale <= 0.4 && p1.xScale > 0.05)
        {
            p1.color = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
        }
        else if(p1.xScale <= 0.05)
        {
            gameover()
        }
    }
}
