//
//  MainScene.swift
//  p06-atari
//
//  Created by Souritra Das Gupta on 4/18/17.
//  Copyright © 2017 Souritra Das Gupta. All rights reserved.
//

import Foundation
import SpriteKit
var targt = 0
var halfway = 0
var final5 = 0
class MainScene: SKScene{
        let startlabel1 = SKLabelNode(fontNamed: "Pixel-Noir Caps")
        let startlabel2 = SKLabelNode(fontNamed: "Pixel-Noir Caps")
        let startlabel3 = SKLabelNode(fontNamed: "Pixel-Noir Caps")
        let startlabel11 = SKLabelNode(fontNamed: "Pixel-Noir Caps")
        let startlabel12 = SKLabelNode(fontNamed: "Pixel-Noir Caps")
        let startlabel13 = SKLabelNode(fontNamed: "Pixel-Noir Caps")
        override func didMove(to view: SKView) {
            let bg = SKSpriteNode(color: #colorLiteral(red: 0.05882352963, green: 0.180392161, blue: 0.2470588237, alpha: 1), size: self.size)
            bg.position = CGPoint(x: self.size.width/2, y: self.size.height/2)
            bg.zPosition = 0;
            let label = SKLabelNode(fontNamed: "Pixel-Noir Caps")
            label.text = "ULTIMATE PONG"
            label.fontColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            label.fontSize = 50
            label.position = CGPoint(x: self.size.width/2, y: self.size.height * 0.8)
            label.zPosition = 1
            let infolabel = SKLabelNode(fontNamed: "Pixel-Noir Caps")
            infolabel.text = "2 Player"
            infolabel.fontColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            infolabel.fontSize = 30
            infolabel.position = CGPoint(x: self.size.width/2, y: label.position.y - 135)
            infolabel.zPosition = 1
            let chooselabel = SKLabelNode(fontNamed: "Pixel-Noir Caps")
            chooselabel.text = "Choose points to start:"
            chooselabel.fontColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            chooselabel.fontSize = 22
            chooselabel.position = CGPoint(x: self.size.width/2, y: infolabel.position.y - 65)
            chooselabel.zPosition = 1
            startlabel1.text = "15"
            startlabel2.text = "50"
            startlabel3.text = "100"
            startlabel1.fontColor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
            startlabel2.fontColor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
            startlabel3.fontColor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
            startlabel1.fontSize = 18
            startlabel2.fontSize = 18
            startlabel3.fontSize = 18
            startlabel1.position = CGPoint(x: self.size.width/2, y: chooselabel.position.y - 65)
            startlabel2.position = CGPoint(x: self.size.width/2, y: startlabel1.position.y - 50)
            startlabel3.position = CGPoint(x: self.size.width/2, y: startlabel2.position.y - 50)
            self.addChild(chooselabel)
            self.addChild(startlabel1)
            self.addChild(startlabel2)
            self.addChild(startlabel3)
            self.addChild(infolabel)
            self.addChild(label)
            self.addChild(bg)
            
            let infolabel1 = SKLabelNode(fontNamed: "Pixel-Noir Caps")
            infolabel1.text = "1 Player"
            infolabel1.fontColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            infolabel1.fontSize = 30
            infolabel1.position = CGPoint(x: self.size.width/2, y: startlabel3.position.y - 135)
            infolabel1.zPosition = 1
            let chooselabel1 = SKLabelNode(fontNamed: "Pixel-Noir Caps")
            //chooselabel1.text = "Survival"
            chooselabel1.fontColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            chooselabel1.fontSize = 22
            chooselabel1.position = CGPoint(x: self.size.width/2, y: infolabel1.position.y - 25)
            chooselabel1.zPosition = 1
            startlabel11.text = "Survival Mode"
            startlabel12.text = "Boundless Mode"
            startlabel13.text = "100"
            startlabel11.fontColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
            startlabel12.fontColor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
            //startlabel13.fontColor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
            startlabel11.fontSize = 18
            startlabel12.fontSize = 18
            //startlabel13.fontSize = 18
            startlabel11.position = CGPoint(x: self.size.width/2, y: chooselabel1.position.y - 65)
            startlabel12.position = CGPoint(x: self.size.width/2, y: startlabel11.position.y - 50)
            //startlabel13.position = CGPoint(x: self.size.width/2, y: startlabel12.position.y - 50)
            
            self.addChild(chooselabel1)
            self.addChild(startlabel11)
            self.addChild(startlabel12)
            //self.addChild(startlabel13)
            self.addChild(infolabel1)
            
        }
        override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
            for t: AnyObject in touches{
                let point = t.location(in: self)
                if startlabel1.contains(point) {
                    targt = 15
                    halfway = 5
                    final5 = 10
                    let toScene = GameScene(size: self.size)
                    toScene.scaleMode = self.scaleMode
                    let move = SKTransition.fade(withDuration: 1)
                    self.view!.presentScene(toScene, transition: move)
                }
                if startlabel2.contains(point) {
                    targt = 50
                    halfway = 25
                    final5 = 40
                    let toScene = GameScene(size: self.size)
                    toScene.scaleMode = self.scaleMode
                    let move = SKTransition.fade(withDuration: 1)
                    self.view!.presentScene(toScene, transition: move)
                }
                if startlabel3.contains(point) {
                    targt = 100
                    halfway = 50
                    final5 = 80
                    let toScene = GameScene(size: self.size)
                    toScene.scaleMode = self.scaleMode
                    let move = SKTransition.fade(withDuration: 1)
                    self.view!.presentScene(toScene, transition: move)
                }
                if startlabel11.contains(point) {
                    targt = 15
                    halfway = 5
                    final5 = 10
                    let toScene = Survival(size: self.size)
                    toScene.scaleMode = self.scaleMode
                    let move = SKTransition.fade(withDuration: 1)
                    self.view!.presentScene(toScene, transition: move)
                }
                if startlabel12.contains(point) {
                    targt = 50
                    halfway = 25
                    final5 = 40
                    let toScene = Nobounds(size: self.size)
                    toScene.scaleMode = self.scaleMode
                    let move = SKTransition.fade(withDuration: 1)
                    self.view!.presentScene(toScene, transition: move)
                }
                /*if startlabel13.contains(point) {
                    targt = 100
                    halfway = 50
                    final5 = 80
                    let toScene = Survival(size: self.size)
                    toScene.scaleMode = self.scaleMode
                    let move = SKTransition.fade(withDuration: 1)
                    self.view!.presentScene(toScene, transition: move)
                }*/
            }
        }
    }

